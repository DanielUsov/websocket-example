// Импортируем библиотеку ws (не Socket.IO)
const webSocket = require('ws');

const PORT = 8080;

// создаем WebSocket Server
const wss = new webSocket.Server({ port: PORT }, () => {
	console.log(`Server started on ${PORT} port`);
});

// подписываемся на событие "подключение"
wss.on('connection', (ws) => {
	ws.on('message', function message(message) {
		const data = JSON.parse(message);
		if (data.type === 'message') {
			wss.clients.forEach((client) => {
				if (client !== ws && client.readyState === webSocket.OPEN) {
					client.send(JSON.stringify({ type: 'message', data: data.data }));
				}
			});
		}
	});
});
