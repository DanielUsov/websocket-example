const messagesE = document.getElementById('messages');
const messageE = document.getElementById('message');
const sendMessageE = document.getElementById('sendMessage');

const ws = new WebSocket('ws://localhost:8080');

ws.onopen = () => {
	console.log('online');
};

ws.onclose = () => {
	console.log('disconnected');
};

ws.addEventListener('message', function (event) {
	const data = JSON.parse(event.data);
	if (data.type === 'message') {
		addMessage(data.data);
	}
});

// отправляем сообщение
function sendMessage() {
	const message = messageE.value;
	if (!message) {
		return false;
	}
	ws.send(JSON.stringify({ type: 'message', data: message }));
	addMessage(message);
	messageE.value = '';
}

// записываем сообщение в "чат"
function addMessage(message) {
	const node = document.createElement('P');
	const text = document.createTextNode(message);
	node.appendChild(text);
	node.classList.add('users__message');
	messagesE.appendChild(node);
}

function checkForEnter(e) {
	if (e.keyCode == 13) {
		document.sendMessageE.click();
	}
}
